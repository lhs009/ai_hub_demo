from flask import Flask, render_template, request, jsonify
from werkzeug.utils import secure_filename
from demo_modi import mask3d_render
import cv2
import os
import time

UPLOAD_DIR = "./static/outputs/"

app = Flask(__name__)
app.config['UPLOAD_DIR'] = UPLOAD_DIR

@app.route('/') # 접속하는 url
def index():
  return render_template('index.html')

@app.route('/outputs', methods=['GET'])
def processed_file():
  fileName = request.args.get('f')
  return app.send_static_file('outputs/' + fileName)

@app.route('/upload', methods=['POST'])
def upload_file():
  print('in')
  f = request.files['image']
  origin_filename = secure_filename(f.filename)
  origin = os.path.join(app.config['UPLOAD_DIR'], origin_filename)
  print(origin)
  f.save(origin)

  # get the 3d_render_image
  masked_img = mask3d_render(origin)
  dest_filename = get_timestamp() + '_' + origin_filename
  print(dest_filename)
  dest = app.config['UPLOAD_DIR'] + dest_filename
  print(dest)
  cv2.imwrite(dest, masked_img)
  
  response = jsonify(result='outputs?f=' +  dest_filename)
  return response

# def get_mask3d_render_image(img_path):
#     mask3d_img = mask3d_render(img_path)
#     # cv2.imwrite('./static/result06.png', mask3d_img)
#     return mask3d_img

def get_timestamp():
  return str(time.time())

if __name__ == "__main__":
    app.run(debug=True)
    print(get_timestamp())
    # encoded_image = encoded_image_to_base64('./uploads/go2.jpg')
    # print(encoded_image)
